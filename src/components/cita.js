import React, { Component } from 'react';
import uuid from 'uuid'

const stado_inicial = {
    cita:{
        mascota: '',
        propietario: '',
        fecha: '',
        hora: '',
        sintomas: ''
      },
      error: false
}
class NuevaCita extends Component {
    
    state = {
        ...stado_inicial
    }
    handleSubmit = e =>{
        e.preventDefault();
        const {mascota, propietario, fecha, hora, sintomas} = this.state.cita;
        if(mascota==='' || propietario==='' || fecha==='' || hora==='' || sintomas===''){
            this.setState({
                error: true
            });
            return;
        }
        let nueva_cita = {...this.state.cita}
        nueva_cita.id = uuid()
        this.props.crear_nueva_cita(nueva_cita)
        
        this.setState({
            ...stado_inicial
        })
        
       
    }
    handleChange = e =>{
        this.setState({
            cita : {
                ...this.state.cita,
                [e.target.name]: e.target.value 
            }
        })
    }
    render() {
        const {error} = this.state
        return (
            <div className="card mt-5 py-5">
                <div className="card-body">
            <h4 className="card-title text-center mb-5">
                Llena el formulario para crear una nueva cita
            </h4>
            {error ? <div className="alert alert-danger mt-2 mb-5 text-center">
                Todos los campos son obligatorios
            </div> :null}
            <form
                onSubmit={this.handleSubmit}
            >
                <div className="from-group row">
                    <label className="col-sm-4 col-lg-4 col-form-label">Nombre de mascota</label>
                    <input type="text"
                            id="mascota"
                            className="form-control"
                            placeholder="Nombre Mascota"
                            name="mascota"
                            onChange= {this.handleChange}
                            value={this.state.cita.mascota}
                    />
                </div> {/* form-group */}
                <div className="from-group row">
                    <label className="col-sm-4 col-lg-4 col-form-label">Nombre de dueño</label>
                    <input type="text"
                            className="form-control"
                            placeholder="Nombre Dueño"
                            name="propietario"
                            value={this.state.cita.propietario}
                            onChange= {this.handleChange}
                    />
                </div> {/* form-group */}
                <div className="from-group row">
                    <label className="col-sm-4 col-lg-4 col-form-label">Fecha</label>
                    <input type="date"
                            className="form-control"
                            name="fecha"
                            value={this.state.cita.fecha}
                            onChange= {this.handleChange}
                    />
                </div> {/* form-group */}
                <div className="from-group row">
                    <label className="col-sm-4 col-lg-4 col-form-label">Hora</label>
                    <input type="time"
                            className="form-control"
                            name="hora"
                            value={this.state.cita.hora}
                            onChange= {this.handleChange}
                    />
                </div> {/* form-group */}
                <div className="from-group row">
                    <label className="col-sm-4 col-lg-4 col-form-label">Sintomas</label>
                    <textarea type="date"
                            className="form-control"
                            name="sintomas"
                            value={this.state.cita.sintomas}
                            onChange= {this.handleChange}
                    ></textarea>
                </div> {/* form-group */}
                <button>Guardar datos</button>
            </form>
            </div>
            </div>
        );
    }
}

export default NuevaCita;