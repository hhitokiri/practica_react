import React from 'react';
import DatePrint from './datePrint';
import PropTypes from 'prop-types';

const DatesList = ({list_dates, delet_date})=>{
    let title = ""
    if(Object.keys(list_dates).length !== 0 ){
        title = "Dates List";
    }else{
        title = "List not exist";
    }
    return(
        <div className="row">
            <div className="col-md-12">
            <h2>{title}</h2>
            </div>   
            
            <div className="col-md-10">
                <ul>{list_dates.map(date =>(
                    <DatePrint
                        key={date.id}
                        date={date}
                        delet_date={delet_date}
                    />
                ))}</ul>
            </div>
        </div>
    )
}
DatesList.propTypes = {
    list_dates: PropTypes.array.isRequired,
    delet_date: PropTypes.func.isRequired
}

export default DatesList;