import React, {Component} from 'react';
import './statics/css/bootstrap.min.css';
import Header from './components/Header';
import NuevaCita from './components/cita'
import DatesList from './components/list_dates';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
class App extends Component{
  state = {
    citas: []
  }

  componentDidMount(){
    if (localStorage.key("citas")){
       this.setState({
         citas : this.state.citas = JSON.parse(localStorage.getItem("citas"))
       })

    }
    
  }

  componentDidUpdate(){
    localStorage.setItem("citas", JSON.stringify(this.state.citas))
  }
  
  crear_nueva_cita = datos =>{
    const citas = [...this.state.citas, datos]
    this.setState({
      citas
    })

  }
  delet_date = date =>{
     const cita = [...this.state.citas]

     const dates = cita.filter(data => data.id !== date);

     this.setState({
       citas: dates
     })

  }
  render(){
    return(
      <div className="container">
        {/* <Router >
        <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
          <li>
            <Link to="/topics">Topics</Link>
          </li>
        </ul>

        <hr />

        <Route exact path="/" component={Header} />
        <Route path="/about" component={NuevaCita} />
        <Route path="/topics" component={DatesList} />
      </div>

          </Router> */}


        <Header 
        titulo='Api de sales force'
        />
        <div className="row">
          <div className="col-md-10 mx-auto">
            <NuevaCita 
            crear_nueva_cita={this.crear_nueva_cita}
            />
          </div>         
        </div>
        <div className="row">
          <div className="col-md-10 mx-auto">
            <DatesList
              list_dates = {this.state.citas}
              delet_date = {this.delet_date}
            />
          </div>
        </div> 
      </div>
    );
  }

}

export default App;
